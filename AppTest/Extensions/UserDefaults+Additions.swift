import Foundation

extension UserDefaults {
    
    func markCharacterAsFavourite(characterId: Int){
        set(characterId, forKey: UserDefaultsKeys.favouriteId)
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(NotificationKeys.favouriteChanged), object: nil)
    }
    
    func getFavouriteCharacterId() -> Int{
        return integer(forKey: UserDefaultsKeys.favouriteId)
    }
}
