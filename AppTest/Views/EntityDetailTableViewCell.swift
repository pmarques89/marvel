//
//  EntityDetailTableViewCell.swift
//  AppTest
//
//  Created by Pedro Marques on 25/03/2018.
//  Copyright © 2018 Pedro Marques. All rights reserved.
//

import UIKit

class EntityDetailTableViewCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    // MARK: Public Properties
    
    var comic : Comic! {
        didSet {
            self.descriptionLabel.text = comic.title ?? "None"
            self.nameLabel.text = comic.description ?? "None"
        }
    }
    
    var event : Event! {
        didSet {
            self.descriptionLabel.text = event.title ?? "None"
            self.nameLabel.text = event.description ?? "None"
        }
    }
    
    var story : Story! {
        didSet {
            self.descriptionLabel.text = story.title ?? "None"
            self.nameLabel.text = story.description ?? "None"
        }
    }
    
    var series : Series! {
        didSet {
            self.descriptionLabel.text = series.title ?? "None"
            self.nameLabel.text = series.description ?? "None"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
