//
//  HeroListTableViewCell.swift
//  AppTest
//
//  Created by Pedro Marques on 24/03/2018.
//  Copyright © 2018 Pedro Marques. All rights reserved.
//

import UIKit

class HeroListTableViewCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroNameLabel: UILabel!
    @IBOutlet weak var favouriteImageView: UIImageView!
    
    // MARK: Public Properties
    
    var hero : Character! {
        didSet {
            self.heroNameLabel.text = hero.name
            self.heroImageView.image = nil // Should be updated in TableView
            self.favouriteImageView.isHidden = UserDefaults.standard.getFavouriteCharacterId() != hero.id
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
