#  Overview

This is a very simple app, filling all the requirements for the Farfetch iOS interview exercise.

The app has two UIViewControllers: HeroListViewController (the initial ViewController) and a HeroDetailViewController.

HeroListViewController is where you can see a list of Marvel Heroes and your favourite one. You can also search for a specific hero name using the search bar at the top. If you want to see the next page of heroes you should scroll down to the end of the page.

In the HeroDetails view controller you can see the comics, events, stories and series. and make a hero your favourite. To do this you must click on the add favourite button. If a hero is already your favourite, another icon is shown.

API requests follow the guide written here:
https://medium.com/makingtuenti/writing-a-scalable-api-client-in-swift-4-b3c6f7f3f3fb

This app was written in Swift because I don't get to use it at my current job, I would like to be more proficient in it, and also to make use of new tools like codable/decodable.
