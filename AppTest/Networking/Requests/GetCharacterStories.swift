import Foundation

public struct GetCharacterStories: APIRequest {
    public typealias Response = [Story]
    
    public var resourceName: String {
        return "characters/\(characterId)/stories"
    }
    
    // Parameters
    public let characterId: Int
    public let limit: Int?
    
    public init(characterId: Int,
                limit: Int? = nil) {
        self.characterId = characterId
        self.limit = limit
    }
}
