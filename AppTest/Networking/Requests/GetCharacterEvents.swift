import Foundation

public struct GetCharacterEvents: APIRequest {
    public typealias Response = [Event]
    
    public var resourceName: String {
        return "characters/\(characterId)/events"
    }
    
    // Parameters
    public let characterId: Int
    public let limit: Int?
    
    public init(characterId: Int,
                limit: Int? = nil) {
        self.characterId = characterId
        self.limit = limit
    }
}
