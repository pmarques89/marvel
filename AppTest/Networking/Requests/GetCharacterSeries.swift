import Foundation

public struct GetCharacterSeries: APIRequest {
    public typealias Response = [Series]
    
    public var resourceName: String {
        return "characters/\(characterId)/series"
    }
    
    // Parameters
    public let characterId: Int
    public let limit: Int?
    
    public init(characterId: Int,
                limit: Int? = nil) {
        self.characterId = characterId
        self.limit = limit
    }
}
