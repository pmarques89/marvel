import Foundation

public struct GetCharacterComics: APIRequest {
    public typealias Response = [Comic]
    
    public var resourceName: String {
        return "characters/\(characterId)/comics"
    }
    
    // Parameters
    public let characterId: Int
    public let limit: Int?
    
    public init(characterId: Int,
                limit: Int? = nil) {
        self.characterId = characterId
        self.limit = limit
    }
}
