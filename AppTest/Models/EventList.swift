import Foundation

public struct EventList: Decodable {
    public let available: Int
    public let returned: Int
    public let collectionURI: String?
    public var items: [EventSummary]?
    
    public init(available: Int,
                returned: Int,
                collectionURI: String?,
                items: [EventSummary]?) {
        self.available = available
        self.returned = returned
        self.collectionURI = collectionURI
        self.items = items
    }
}
