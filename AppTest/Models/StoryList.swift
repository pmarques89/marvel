import Foundation

public struct StoryList: Decodable {
    public let available: Int
    public let returned: Int
    public let collectionURI: String?
    public let items: [StorySummary]?
    
    public init(available: Int,
                returned: Int,
                collectionURI: String?,
                items: [StorySummary]?) {
        self.available = available
        self.returned = returned
        self.collectionURI = collectionURI
        self.items = items
    }
}
