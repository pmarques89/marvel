import Foundation

public struct Character: Decodable {
    public let id: Int
    public let name: String?
    public let description: String?
    public let thumbnail: Image?
    public let comics: ComicList?
    public let stories: StoryList?
    public let events: EventList?
    public let series: SeriesList?
    
    public init(id: Int = 0,
                name: String? = nil,
                description: String? = nil,
                thumbnail: Image? = nil,
                comics: ComicList? = nil,
                stories: StoryList? = nil,
                events: EventList? = nil,
                series: SeriesList? = nil) {
        self.id = id
        self.name = name
        self.description = description
        self.thumbnail = thumbnail
        self.comics = comics
        self.stories = stories
        self.events = events
        self.series = series
    }
}
