import Foundation

public struct SeriesList: Decodable {
    public let available: Int
    public let returned: Int
    public var collectionURI: String?
    public var items: [SeriesSummary]?
    
    public init(available: Int,
                returned: Int,
                collectionURI: String?,
                items: [SeriesSummary]?) {
        self.available = available
        self.returned = returned
        self.collectionURI = collectionURI
        self.items = items
    }
}
