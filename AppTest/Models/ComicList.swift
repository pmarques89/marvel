import Foundation

public struct ComicList: Decodable {
    public let available: Int
    public let returned: Int
    public let collectionURI: String?
    public let items: [ComicSummary]?
    
    public init(available: Int,
                returned: Int,
                collectionURI: String?,
                items: [ComicSummary]) {
        self.available = available
        self.returned = returned
        self.collectionURI = collectionURI
        self.items = items
    }
}
