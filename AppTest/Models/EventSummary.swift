import Foundation

public struct EventSummary: Decodable {
    public let name: String?
    public let resourceURI: String?
    
    public init (name: String?,
                 resourceURI: String?) {
        self.name = name
        self.resourceURI = resourceURI
    }
}
