import Foundation

public struct StorySummary: Decodable {
    public let name: String?
    public let resourceURI: String?
    public let type: String?
    
    public init (name: String?,
                 resourceURI: String?,
                 type: String?) {
        self.name = name
        self.resourceURI = resourceURI
        self.type = type
    }
}
