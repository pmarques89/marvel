import UIKit

class HeroListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, UISearchBarDelegate {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var superHeroTableView: UITableView!
    @IBOutlet weak var heroSearchBar: UISearchBar!
    
    // MARK: Private Properties
    
    private var results:[Character] = []
    private var isUpdating = false
    private var currentOffset = 0
    private let customPresentAnimationController = CustomAnimationController()
    
    // MARK: View Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCharacters()
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(HeroListViewController.updateFavouriteStatus), name: Notification.Name(NotificationKeys.favouriteChanged), object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HeroDetailSegue",
            let destinationViewController = segue.destination as? HeroDetailViewController {
            if let index = superHeroTableView.indexPathForSelectedRow?.row {
                destinationViewController.hero = results[index]
            }
            destinationViewController.transitioningDelegate = self
        }
    }
    
    // MARK: UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! HeroListTableViewCell
        let character = results[indexPath.row]
        
        cell.hero = character
        updateImageForCell(cell: cell, inTableView: tableView, imageURL: results[indexPath.row].thumbnail?.url, atIndexPath: indexPath)
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentLarger = (scrollView.contentSize.height > scrollView.frame.size.height)
        let viewableHeight = contentLarger ? scrollView.frame.size.height : scrollView.contentSize.height
        let atBottom = (scrollView.contentOffset.y >= scrollView.contentSize.height - viewableHeight + 50)
        if atBottom {
            getCharacters()
        }
    }
    
    // MARK: UISearchBar Delegate Methods

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        clearAndReloadData()
    }
    
    // MARK: UIViewController Transitioning Delegate Methods

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentAnimationController
    }
    
    // MARK: Private Methods
    
    @objc func updateFavouriteStatus() {
        superHeroTableView.reloadData()
    }
    
    func updateImageForCell(cell: UITableViewCell, inTableView tableView: UITableView, imageURL: URL?, atIndexPath indexPath: IndexPath) {
        
        if let imageURL = results[indexPath.row].thumbnail?.url {
            ImageClient.downloadImage(url: imageURL) { (image, error) in
                
                if let heroCell = tableView.cellForRow(at: indexPath) as? HeroListTableViewCell {
                    heroCell.heroImageView.image = image
                }
            }
        }
    }
    
    func getSearchText() -> String? {
        
        if let searchText = self.heroSearchBar.text, !searchText.isEmpty {
            return searchText
        } else {
            return nil
        }
    }
    
    func getCharacters() {
        
        if !isUpdating {
            
            superHeroTableView.showLoadingFooter()
            isUpdating = true
            
            MarvelAPIClient.sharedInstance.send(GetCharacters(name: nil,
                                                              nameStartsWith: getSearchText(),
                                                              limit: 20,
                                                              offset: self.currentOffset))
            { [weak self] response in
                
                self?.isUpdating = false
            
                //                weakself
                DispatchQueue.main.async {
                    self?.superHeroTableView.hideLoadingFooter()
                }
    
                switch response {
                case .success(let dataContainer):
                    self?.results += dataContainer.results
                    self?.currentOffset += 20
                    
                    DispatchQueue.main.async {
                        self?.superHeroTableView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func clearAndReloadData() {
        self.results = []
        self.isUpdating = false
        self.currentOffset = 0
        self.superHeroTableView.reloadData()
        self.getCharacters()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: NotificationKeys.favouriteChanged), object: nil)
    }

}
