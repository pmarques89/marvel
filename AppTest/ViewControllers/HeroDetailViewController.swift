import UIKit

class HeroDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var heroDetailTableView: UITableView!
    @IBOutlet weak var favouriteImageView: UIImageView!
    
    // MARK: Private Properties
    
    var comics : [Comic] = []
    var events : [Event] = []
    var stories : [Story] = []
    var series : [Series] = []
    
    // MARK: Public Properties
    
    var hero : Character = Character()
    
    // MARK: View Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heroDetailTableView.rowHeight = UITableViewAutomaticDimension
        heroDetailTableView.estimatedRowHeight = 50
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(updateFavouriteIcon), name: Notification.Name(NotificationKeys.favouriteChanged), object: nil)
        
        updateFavouriteIcon()
        getComics()
        getEvents()
        getStories()
        getSeries()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: NotificationKeys.favouriteChanged), object: nil)
    }
    
    // MARK: IBActions
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favouriteButtonTapped(_ sender: Any) {
        
        if UserDefaults.standard.getFavouriteCharacterId() != hero.id {
            UserDefaults.standard.markCharacterAsFavourite(characterId: hero.id)
        }
    }
    
    // MARK: UITableView Delegate Methods

    func numberOfSections(in tableView: UITableView) -> Int {
        
        var nSections = 0
        
        if (hero.comics?.items?.count) != nil {
            nSections += 1
        }
        
        if (hero.events?.items?.count) != nil {
            nSections += 1
        }
        
        if (hero.stories?.items?.count) != nil {
            nSections += 1
        }
        
        if (hero.series?.items?.count) != nil {
            nSections += 1
        }
        
        return nSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return comics.count
            
        case 1:
            return events.count
            
        case 2:
            return stories.count
            
        case 3:
            return series.count
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Comics"
            
        case 1:
            return "Events"
            
        case 2:
            return "Stories"
            
        case 3:
            return "Series"
            
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EntityDetailCell", for: indexPath) as! EntityDetailTableViewCell
        
        switch indexPath.section {
        case 0:
            let comic = comics[indexPath.row]
            cell.comic = comic
            self.heroDetailTableView.beginUpdates()
            self.heroDetailTableView.endUpdates()
        case 1:
            let event = events[indexPath.row]
            cell.event = event
            self.heroDetailTableView.beginUpdates()
            self.heroDetailTableView.endUpdates()
        case 2:
            let story = stories[indexPath.row]
            cell.story = story
            self.heroDetailTableView.beginUpdates()
            self.heroDetailTableView.endUpdates()
        case 3:
            let serie = series[indexPath.row]
            cell.series = serie
            self.heroDetailTableView.beginUpdates()
            self.heroDetailTableView.endUpdates()
        default:
            return cell
        }
        
        return cell
    }
    
    // MARK: Private Methods

    @objc private func updateFavouriteIcon() {
        if UserDefaults.standard.getFavouriteCharacterId() == hero.id {
            favouriteImageView.image = UIImage(named:"ic_favourite")
        } else {
            favouriteImageView.image = UIImage(named:"ic_favourite_add")
        }
    }
    
    private func getComics() {
        MarvelAPIClient.sharedInstance.send(GetCharacterComics(characterId:self.hero.id, limit: 3)) { [weak self] response in
            switch response {
            case .success(let dataContainer):
                self?.comics = dataContainer.results
                DispatchQueue.main.async {
                    self?.heroDetailTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getEvents() {
        MarvelAPIClient.sharedInstance.send(GetCharacterEvents(characterId:self.hero.id, limit: 3)) { [weak self] response in
            switch response {
            case .success(let dataContainer):
                self?.events = dataContainer.results
                DispatchQueue.main.async {
                    self?.heroDetailTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getStories() {
        MarvelAPIClient.sharedInstance.send(GetCharacterStories(characterId:self.hero.id, limit: 3)) { [weak self] response in
            switch response {
            case .success(let dataContainer):
                self?.stories = dataContainer.results
                DispatchQueue.main.async {
                    self?.heroDetailTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getSeries() {
        MarvelAPIClient.sharedInstance.send(GetCharacterSeries(characterId:self.hero.id, limit: 3)) { [weak self] response in
            switch response {
            case .success(let dataContainer):
                self?.series = dataContainer.results
                DispatchQueue.main.async {
                    self?.heroDetailTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
