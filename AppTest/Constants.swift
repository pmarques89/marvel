import Foundation

enum NotificationKeys {
    static let favouriteChanged = "FavouriteChanged"
}

enum UserDefaultsKeys {
    static let favouriteId = "FavouriteID"
}
